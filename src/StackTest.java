import jstack.LLImplementation.LinkedStack;
import jstack.arrayImplementation.ArrayStack;

public class StackTest {
    public static void main(String[] args) {
        ArrayStack<Integer> myStack = new ArrayStack<>();
        /* testing singular push, pop and top */

        System.out.println("Stack empty: " + myStack.isEmpty());
        System.out.println("Adding 22 to stack");
        myStack.push(22);
        System.out.println("Item on top is: " + myStack.top());
        System.out.println("Adding item 4 to stack");
        myStack.push(4);
        System.out.println("Item on top is: " + myStack.top());
        System.out.println("Removing item from stack");
        myStack.pop();
        System.out.println("item on top of stack -: " + myStack.top());


        /* Testing multiple pushes and pops */
        for(int i=0; i!=30;i++){
            myStack.push(i);
        }
        System.out.println("Item on top of stack -: " + myStack.top());


        LinkedStack<String> myList = new LinkedStack<>();
        System.out.println(myList.isEmpty());
        System.out.println("Adding to stack -");
        myList.push("first");
        System.out.println(myList.top());
        myList.push("second");
        System.out.println(myList.top());
        myList.push("third");
        System.out.println(myList.top());
        System.out.println("Testing stack pop -: ");
        myList.pop();
        System.out.println(myList.top());
    }
}
