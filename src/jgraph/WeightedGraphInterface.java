package jgraph;

public interface WeightedGraphInterface <T>{
    void addVertex(T vertex);
    void addEdge(T fromVertex, T toVertex,int weight);
    int weightIs(T fromVertex, T toVertex);
    boolean isEmpty();
    boolean isFull();
    boolean hasVertex(T vertex);
}
