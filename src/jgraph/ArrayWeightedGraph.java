package jgraph;

public class ArrayWeightedGraph<T> implements WeightedGraphInterface<T> {
    public static final int NULL_EDGE = 0;
    //Default capacity of a graph
    public static final int DEFCAP = 50;
    // Used to keep track of the current amount of vertexes
    private int numVercities;
    private int maxVercities;
    private T[] vercities;
    private int[][] edges;
    private boolean marks[];

    /*
        Non-argument constructor to give the graph a default value of
        50 vertexes
    */
    public ArrayWeightedGraph(){
        this.maxVercities = DEFCAP;
        this.numVercities = 0;
        this.vercities = (T[]) new Object[DEFCAP];
        this.edges = new int[DEFCAP][DEFCAP];
        this.marks = new boolean[DEFCAP];
    }

    /*
        int argument constructor used to give a prefered value for the
        amount of vertexes
    */
    public ArrayWeightedGraph(int numV){
        this.maxVercities = numV;
        this.numVercities = numV;
        this.vercities = (T[]) new Object[maxVercities];
        this.edges = new int[maxVercities][maxVercities];
        this.marks = new boolean[maxVercities];
    }

    @Override
    public boolean isEmpty() {
        if(numVercities == 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean isFull() {
        if(numVercities == maxVercities)
            return true;
        return false;
    }
    /*
        Checks to see if the requested vertex is in the graph
        returns either true or false depending on the outcome
    */
    @Override
    public boolean hasVertex(T vertex) {
        boolean isEqual = false;
        while(!isEqual){
            for(int i = 0; i<maxVercities;i++){
                if(vertex.equals(vercities[i]))
                    isEqual = true;
            }
        }
        return isEqual;
    }

    /*
                    Adds a vertex to the graph
                */
    @Override
    public void addVertex(T vertex) {
        // Add the vertex to the array
        vercities[numVercities] = vertex;
        for(int i=0; i<numVercities;i++){
            // Set any edges to null since its a new vertex
            edges[numVercities][i] = NULL_EDGE;
            edges[i][numVercities] = NULL_EDGE;
        }
        // update the total
        numVercities ++;
    }
    /*
        Returns the edge between two existing vertexes
    */
    @Override
    public int weightIs(T fromVertex, T toVertex) {
        int row;
        int col;
        row = findIndex(fromVertex);
        col = findIndex(toVertex);
        return edges[row][col];
    }

    /*
            Adds an edge between two vertexes
            Assumes that the two vertexes are in the graph already
        */
    @Override
    public void addEdge(T fromVertex, T toVertex, int weight) {
        int row;
        int col;
        row = findIndex(fromVertex);
        col = findIndex(toVertex);
        edges[row][col] = weight;
    }

    /*
        Helper method used to find the index of a vertex
        Vertex must be in the array for it to work
    */
    private int findIndex(T vertex){
        int index = 0;
        while(!vertex.equals(vercities[index])){
            index++;
        }
        return index;
    }
}
