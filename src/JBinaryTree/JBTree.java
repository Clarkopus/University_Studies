package JBinaryTree;

/*
    My implementation of a Binary  Tree
*/

public class JBTree {
    private TreeNode node;
    public JBTree(TreeNode node){
        this.node = node;
    }

    public TreeNode getNode(){
        return node;
    }

    /* need to fix insert method. Been a right tool and made it only edit the left and right children
    * of the root node. Easy fix, know what to do, just very late to fix. Nighty night me.*/
    public void insert (int input)throws TreeIsEmptyException{
        TreeNode myNode = new TreeNode(input,null,null);
        if(node == null){
            throw new TreeIsEmptyException("Tree is empty!");
        }

        if(node.getData() < input){
            node.setRightChild(input);
        }

        else if(node.getData() > input){
            node.setLeftChild(input);
        }
    }

    public void preOrderTraversal() throws TreeIsEmptyException{
        if(node == null){
            throw new TreeIsEmptyException("Tree is empty!");
        }

        TreeNode myNode = node;
        searchHelper(node);

    }

    private void searchHelper(TreeNode n) throws TreeIsEmptyException{

        if(n == null){
            return;
        }
        System.out.println(n.getData());
        searchHelper(n.getLeftChild());
        searchHelper(n.getRightChild());

    }
}
