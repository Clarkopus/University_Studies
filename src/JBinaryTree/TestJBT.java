package JBinaryTree;

public class TestJBT {
    public static void main(String[] args) {
        JBTree myTree = new JBTree(new TreeNode(50,null,null));

        try {
            myTree.insert(61);
            myTree.insert(10);
            myTree.insert(5);
            myTree.insert(11);
            myTree.preOrderTraversal();
        }catch (TreeIsEmptyException e){
            System.out.println("Error in method insert - " + e.getMessage());
        }

    }
}
