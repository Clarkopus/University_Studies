package JBinaryTree;

public class BinarySearchTree {

    private TreeNode root;

    public BinarySearchTree(){
        this.root = null;
    }

    public boolean insert(Integer i){

        TreeNode daddy = root;
        TreeNode child = daddy;
        boolean goneLeft = false;

        while(child != null && i.compareTo(child.getData()) != 0){
            daddy = child;
            if(i.compareTo(child.getData()) <0){
                goneLeft = true;
                child = child.getLeftChild();
            }
            if (i.compareTo(child.getData()) > 0){
                goneLeft = false;
                child = child.getRightChild();
            }
        }
        // If the child != null then the number already exists
        if(child == null){
            return false;
        }
        // Else assume the number isn't in the tree
        else{
            TreeNode leaf = new TreeNode(i,null,null);
            // If the parent is null then the tree must be empty
            if(daddy == null){
                root = leaf;
            }
            if(goneLeft){
                daddy.setLeftChild(child);
            }
            if(!goneLeft){
                daddy.setRightChild(child);
            }
            return true;
        }

    }

    public boolean find(Integer i){
        // Make node to reference root
        TreeNode node = root;
        boolean found = false;
        // While the bottom of the tree isn't reached AND the check is false
        while(node!=null && found == false){
            // Compare the integer value to the node's data
            int compare = i.compareTo(node.getData());
            // if i = 0 - The value was found
            if(compare ==0){
                found = true;
            }
            // If i < 0 - The value is smaller than the node's
            else if(compare <0){
                node = node.getLeftChild();
            }
            // Else if i > 0 - The value is larger than the node's
            else{
                node = node.getRightChild();
            }
        }
        return found;
    }

    // delete only works if the node is a leaf. Needs more work... And to be less shit.
    public boolean delete(Integer i){
        boolean found = false;
        TreeNode parent = root;
        TreeNode node = parent;
        boolean goneLeft = false;
        while(node!=null && found !=true) {
            parent = node;
            if(node.getLeftChild().getData() == i){
                found = true;
                goneLeft = true;
            }
            else if(node.getRightChild().getData() == i)
                found = true;
            else{
                if(i.compareTo(node.getData()) < 0)
                    node = node.getLeftChild();
                else
                    node = node.getRightChild();
            }
        }
        if(!found)
            return false;
        else{
            if(node.getLeftChild() == null && node.getRightChild() == null){
                if(goneLeft){
                    parent.setLeftChild(null);
                    return true;
                }
                else{
                    parent.setRightChild(null);
                    return true;
                }
            }
        }
        return false;
    }
}
