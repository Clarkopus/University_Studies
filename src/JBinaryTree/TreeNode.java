package JBinaryTree;
/*
    Tree nodes that model each node in a Binary Search tree
    Each node contains one left child and one right child
    A node also contains a value, in this case a generic value.
*/

public class TreeNode {
    private int data;
    private TreeNode lChild;
    private TreeNode rChild;
    public TreeNode(int data, TreeNode lChild, TreeNode rChild){
        this.data = data;
        this.lChild = lChild;
        this.rChild = rChild;
    }

    public int getData(){
        return data;
    }

    public TreeNode getLeftChild(){
        return lChild;
    }

    public void setLeftChild(TreeNode node){this.lChild = node;}
    public void setLeftChild(int item){
        lChild = new TreeNode(item,null,null);
    }

    public TreeNode getRightChild(){
        return rChild;
    }

    public void setRightChild(TreeNode node){this.rChild = node;}
    public void setRightChild(int item){
        new TreeNode(item,null,null);
    }
}
