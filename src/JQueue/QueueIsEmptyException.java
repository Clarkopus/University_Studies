package JQueue;

public class QueueIsEmptyException extends Exception{

    public QueueIsEmptyException(){
        super();
    }

    public QueueIsEmptyException(String message){
        super(message);
    }
}
