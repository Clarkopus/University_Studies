package JQueue;

public interface Queueable<T> {

    public boolean isEmpty();
    public void add(T Item);
    public void removeFront();
    public T front();
}
