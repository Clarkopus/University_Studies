package JQueue.arrayImplementation;

import JQueue.QueueIsEmptyException;
import JQueue.Queueable;

public class ArrayQueue<T> implements Queueable<T> {

    int frontPosition;
    int backPosition;
    T[] arr;

    public ArrayQueue(){
        backPosition = -1;
        frontPosition = 0;
        this.arr = (T[]) new Object[20];
    }

    @Override
    public boolean isEmpty() {
        if(frontPosition == (backPosition+1)%arr.length){
            return true;
        }
        return false;
    }

    @Override
    public T front() {
        try {
            if (isEmpty())
                throw new QueueIsEmptyException("Queue is empty!");
        }catch (QueueIsEmptyException e){
            System.out.println("Error in method front() - " + e.getMessage());
        }
        return arr[frontPosition];
    }

    @Override
    public void add(T item) {
        try {
            if (isEmpty())
                throw new QueueIsEmptyException("Queue is empty");
        }catch (QueueIsEmptyException e){
            System.out.println("Error in method add() - " + e.getMessage());
        }
        if(backPosition == arr.length-1){
            T[] temp = (T[])new Object[arr.length+20];
            for(int i=0;i<arr.length;i++){
                temp[i] = arr[i];
            }
            arr = temp;
        }

        else{
            backPosition++;
            arr[backPosition] = item;
        }
    }

    @Override
    public void removeFront() {
        try{
            if(isEmpty())
                throw new QueueIsEmptyException("Queue is empty");
        }catch (QueueIsEmptyException e){
            System.out.println("Error in method removeFront() - " + e.getMessage());
        }
        System.out.println("Removing item: " + arr[frontPosition]);
        arr[frontPosition] = null;
        frontPosition++;
    }

    public String toString(){
        String returnString = "";
        for(int i= frontPosition; i < backPosition;i++){
            returnString += "|" + arr[i] + "| ";
        }
        return returnString;
    }
}
