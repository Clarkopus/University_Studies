import JQueue.arrayImplementation.ArrayQueue;

public class QueueTest {

    public static void main(String[] args) {
        ArrayQueue<Integer> testQueue = new ArrayQueue<>();

        System.out.println("Starting testing -: ");
        System.out.println("Testing if isEmpty() returns correctly -: " + testQueue.isEmpty());
        System.out.println("Adding items 2, 3 and 4 to the queue");
        testQueue.add(2);
        testQueue.add(3);
        testQueue.add(4);
        System.out.println("Testing if isEmpty() returns correctly -: " + testQueue.isEmpty());
        System.out.println("Front item is - " + testQueue.front());

        System.out.println("Removing the front item");
        testQueue.removeFront();
        System.out.println("New front is: " + testQueue.front());
        System.out.println("Testing to see if it can add 100 items -: ");
        for (int i = 1; i < 101; i++) {
           testQueue.add(i);
        }
        System.out.println(testQueue.toString());

    }
}
