package jstack;

public class StackIsEmptyException extends Exception{
    public StackIsEmptyException(){
        super();
    }
    public StackIsEmptyException(String message){
        super(message);
    }
}