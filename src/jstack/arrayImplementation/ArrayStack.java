package jstack.arrayImplementation;

import jstack.Stackable;
import jstack.StackIsEmptyException;
public class ArrayStack<T> implements Stackable<T> {

    T[] arr;
    int curser;
    public ArrayStack(){
        // I hate Java generics >:c
        this.arr = (T[]) new Object[20];
        this.curser = -1;
    }

    @Override
    public boolean isEmpty() {
        if(curser == -1)return true;
        return false;
    }

    @Override
    public T top(){
        try {
            if (this.isEmpty()) throw new StackIsEmptyException("Stack is empty");
        }catch(StackIsEmptyException e){
            System.out.println("Error in method top -: " + e.getMessage());
        }
        return arr[curser];
    }

    @Override
    public void push(T item) {
        curser++;
        if(curser == arr.length){
            T[] temp = (T[])new Object[arr.length+20];
            for (int i = 0; i < arr.length; i++) {
                temp[i] = arr[i];
            }
            arr = temp;
        }
        arr[curser] = item;
    }

    @Override
    public void pop() {
        try{
            if(this.isEmpty()) throw new StackIsEmptyException("Stack is empty");
        }catch (StackIsEmptyException e ){
            System.out.println("Error is method pop -: " + e.getMessage());
        }

        arr[curser] = null;
        curser--;
    }
}

