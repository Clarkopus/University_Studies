package jstack.LLImplementation;

import jstack.StackIsEmptyException;
import jstack.Stackable;

import java.util.Stack;

public class LinkedStack<T> implements Stackable<T>{
    LinkNode<T> top;

    public LinkedStack(){

    }

    @Override
    public boolean isEmpty() {
        if(top == null)
            return true;
        return false;
    }

    @Override
    public void pop() {
        try {
            if (top == null) {
                throw new StackIsEmptyException("Stack is empty");
            }
        }catch(StackIsEmptyException e){
            System.out.println("Error is method pop -: " + e.getMessage());
        }
        top = top.next;
    }

    @Override
    public T top() {
        return top.getValue();
    }

    @Override
    public void push(T item) {
        top = new LinkNode<>(item,top);
    }
}
