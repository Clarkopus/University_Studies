package jstack.LLImplementation;

/*
    A link node that models a node inside a linked list
*/

public class LinkNode<T> {
    LinkNode<T> next;
    T value;

    public LinkNode(T val,LinkNode<T> ref){
        this.next = ref;
        this.value = val;
    }

    public T getValue(){
        return value;
    }
}
