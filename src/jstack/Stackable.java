package jstack;
/*
    An interface that provides methods for a stack ADT
*/
public interface Stackable<T> {
    // Returns true if stack is empty
    boolean isEmpty();
    // Returns the top item of a stack of type T
    T top();
    // Pushes an item to the top of the stack
    void push(T item);
    // Removes an item from the top of the stack
    void pop();

}
